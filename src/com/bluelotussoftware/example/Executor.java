/*
 * Copyright 2011-2013 John Yeary <jyeary@bluelotussoftware.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example;

import java.util.concurrent.CountDownLatch;

/**
 *
 * @author John Yeary
 * @version 1.0
 */
public class Executor {

    final static int MAX_COUNT = 10;

    public static void main(String[] args) throws InterruptedException {

        System.out.println("starting counter: " + StaticCounter.getCounter());

        for (int i = 0; i < MAX_COUNT; i++) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    StaticCounter.addToCounter(1);
                }
            }).start();
        }

        // This will return an inaccurate value most of the time. To get an accurate result use a countdown latch
        System.out.println("=====> final counter: " + StaticCounter.getCounter());

        CountDownLatch start = new CountDownLatch(1);
        CountDownLatch finished = new CountDownLatch(MAX_COUNT);

        for (int i = 0; i < MAX_COUNT; ++i) {
            new Thread(new Runner(start, finished)).start();
        }
        System.out.println("Starting the countdown...");
        start.countDown(); // Starting all threads... way cool!
        finished.await();  // Waiting for all threads to finish
        System.out.println("=====> final count on Runner: " + Runner.count);
    }
}
