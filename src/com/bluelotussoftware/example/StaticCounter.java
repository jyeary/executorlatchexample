/*
 * Copyright 2011-2013 John Yeary <jyeary@bluelotussoftware.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example;

/**
 *
 * @author John Yeary
 * @version 1.0
 */
public class StaticCounter {

    private static int counter = 0;

    public StaticCounter() {
    }

    public static synchronized void addToCounter(final int number) {
        counter = counter + number;
        System.out.println("counter value: " + counter);
    }

    public static synchronized int getCounter() {
        return counter;
    }
}
